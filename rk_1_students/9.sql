-- Найти имя и класс студентов, которые нравятся более чем 1 другому студенту.

SELECT H.name, H.grade
FROM likes L
JOIN highschooler H ON l.id2 = H.id
GROUP BY H.name, H.grade, H.id
HAVING COUNT(L.id1) > 1