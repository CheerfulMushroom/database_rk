-- Для каждого студента A, которому нравится студент B, и они не друзья, найти есть ли у них общий друг.
-- Для каждой такой тройки вернуть имя и класс A, B, и C.

WITH LNF AS (
    SELECT L.id1 AS A, L.id2 AS B
    FROM likes L
             LEFT JOIN friend F on L.id2 = F.id1 AND F.id2 = L.id1
    GROUP BY L.id1, L.id2
    HAVING COUNT(F.id1) = 0
)

SELECT H1.name, H1.grade, H2.name, H2.grade, H3.name, H3.grade
FROM LNF
JOIN friend F1 ON LNF.A = F1.id1
JOIN friend F2 on LNF.B = F2.id1 AND F1.id2 = F2.id2
JOIN highschooler H1 on LNF.A = H1.id
JOIN highschooler H2 on LNF.B = H2.id
JOIN highschooler H3 on F1.ID2 = H3.id;

-- ANOTHER ANSWER
select h1.name, h1.grade, h2.name, h2.grade, h3.name, h3.grade
from likes l
         join friend f1 on (l.id1 = f1.id1) /* смотрим друзей каждого */
         join friend f2 on (l.id2 = f2.id1) /* смотрим друзей каждого */
         join highschooler h1 on (l.id1 = h1.id) /* инфа чувака а */
         join highschooler h2 on (l.id2 = h2.id) /* инфа чувака б */
         join highschooler h3 on (f1.id2 = h3.id) /* инфа общего друга */
where f1.id2 = f2.id2 /* есть общий друг */
  and l.id1 not in ( /* не во фреднзоне */
    select f.id2
    from friend f
    where f.id1 = l.id2
);