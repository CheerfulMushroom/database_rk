-- Найти разницу между числом учащихся и числом различных имен.
SELECT COUNT(*) - COUNT(DISTINCT name)
FROM highschooler;

-- ANOTHER ANSWER
select count(*) - count(distinct highschooler.name) as diff
from highschooler;