-- Для каждой ситуации, когда студенту A нравится студент B, но B никто не нравится, вывести имена и классы A и B.

SELECT A.name, A.grade, B.name, B.grade
FROM likes L1
LEFT JOIN likes L2 on L1.id2 = L2.id1
JOIN highschooler A on L1.id1 = A.id
JOIN highschooler B on L1.id2 = B.id
WHERE L2 is NULL;


-- ANOTHER ANSWER

select h1.name, h1.grade, h2.name, h2.grade
from highschooler h1
         join likes l1 on (h1.id = l1.id1)
         left join likes l2 on (l1.id2 = l2.id1)
         join highschooler h2 on (l1.id2 = h2.id)
where l2.id1 isnull;
