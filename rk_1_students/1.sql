-- Найти имена всех студентов кто дружит с кем-то по имени Gabriel.

SELECT GabFriends.name
FROM friend AS F
JOIN highschooler AS GabFriends ON (GabFriends.id = F.id1)
JOIN highschooler AS Gabs ON (Gabs.id = F.id2)
WHERE Gabs.name = 'Gabriel'
