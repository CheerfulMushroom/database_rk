-- Для всех случаев, когда А нравится В, а В нравится С - получите имена и классы А, В и С.

SELECT A.name, A.grade, B.name, B.grade, C.name, C.grade
FROM likes L1
JOIN likes L2 ON L1.id2 = L2.id1 AND L2.id2 <> L1.id1
JOIN highschooler A ON L1.id1 = A.id
JOIN highschooler B ON L1.id2 = B.id
JOIN highschooler C ON L2.id2 = C.id