-- Для каждой пары студентов, которые нравятся друг другу взаимно вывести имя и класс обоих студентов.
-- Включать каждую пару только 1 раз с именами в алфавитном порядке.



SELECT H1.name, H1.grade, H2.name, H2.grade
FROM likes L1
JOIN likes L2 ON L1.id2 = L2.id1 AND L1.id1 = L2.id2 AND L1.id1 < L1.id2
JOIN highschooler H1 on L1.id1 = H1.id
JOIN highschooler H2 on L1.id2 = H2.id
ORDER BY H1.name, H2.name;


