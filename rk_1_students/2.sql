-- Для всех студентов, кому понравился кто-то на 2 или более классов младше,
-- чем он вывести имя этого студента и класс, а так же имя и класс студента который ему нравится.

SELECT LIKES.name, LIKES.grade, LIKED.name, LIKED.grade
FROM likes as L
JOIN highschooler AS LIKES on L.id1 = LIKES.id
JOIN highschooler AS LIKED on L.id2 = LIKED.id
WHERE LIKES.grade - LIKED.grade >= 2;

