-- Найти имена и классы, которые имеют друзей только в том же классе.
-- Вернуть результат, отсортированный по классу, затем имени в классе.

SELECT H.name, H.grade
FROM highschooler H
JOIN friend F ON h.id = f.id1
LEFT JOIN highschooler O ON F.id2 = O.id AND H.grade <> O.grade
GROUP BY H.grade, H.name, H.id
HAVING COUNT(O.id) = 0
ORDER BY H.grade, H.name;

-- ANOTHER ANSWER
select h1.name, h1.grade
FROM highschooler h1
         JOIN friend f1 ON (h1.Id = f1.Id1)
         LEFT JOIN highschooler AS h2 ON (F1.Id2 = h2.Id AND H2.Grade <> h1.Grade)
GROUP BY H1.grade, H1.name, H1.id
HAVING COUNT(h2.Id) = 0
ORDER BY H1.grade, H1.name;
