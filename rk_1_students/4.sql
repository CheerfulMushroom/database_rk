-- Найти всех студентов, которые не встречаются в таблице лайков (никому не нравится и ему никто не нравится),
-- вывести их имя и класс. Отсортировать по классу, затем по имени в классе.

SELECT H.name, H.grade
FROM highschooler AS H
LEFT JOIN likes AS L1 ON L1.id1 = H.id
LEFT JOIN likes AS L2 ON L2.id2 = H.id
WHERE L1 IS NULL AND L2 IS NULL
ORDER BY H.grade, H.name;