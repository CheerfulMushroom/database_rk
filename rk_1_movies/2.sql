-- Найти года в которых были фильмы с рейтингом не ниже 4 и отсортировать по возрастанию.

-- Решение с подзапросом
SELECT DISTINCT m.year
FROM movie AS m
WHERE m.mid IN (
    SELECT DISTINCT r.mid
    FROM rating AS r
    WHERE r.stars >= 4
)
ORDER BY m.year;

-- Решение без подзапроса
SELECT DISTINCT year
FROM rating
JOIN movie on rating.mid = movie.mid
WHERE rating.stars >= 4
ORDER BY year;