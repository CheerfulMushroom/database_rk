-- Найти разницу между средней оценкой фильмов выпущенных до 1980 года,
-- а средней оценкой фильмов выпущенных после 1980 года (фильмы выпущенные в 1980 году не учитываются).
--
-- Убедитесь, что для расчета используете среднюю оценку для каждого фильма.
-- Не просто среднюю оценку фильмов до и после 1980 года.

SELECT AVG(before) - AVG(after) as result
FROM (
         SELECT AVG(CASE WHEN movie.year > 1980 THEN rating.stars ELSE NULL END) as before,
                AVG(CASE WHEN movie.year < 1980 THEN rating.stars ELSE NULL END) as after
         FROM movie
                  JOIN rating USING (mid)
         GROUP BY mid
     ) AS movies;