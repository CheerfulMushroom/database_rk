-- Для каждого фильма, выбрать название и “разброс оценок”,
-- то есть, разницу между самой высокой и самой низкой оценками для этого фильма.
-- Сортировать по “разбросу оценок” от высшего к низшему, и по названию фильма.

SELECT movie.title, max(rating.stars) - min(rating.stars) as star_diff
FROM movie
         JOIN rating USING (mid)
GROUP BY movie.title
ORDER BY star_diff DESC, movie.title;