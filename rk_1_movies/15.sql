-- Некоторые режиссеры сняли более чем один фильм.
-- Для всех таких режиссеров, выбрать названия всех фильмов режиссера, его имя.
-- Сортировка по имени режиссера.
--
-- Пример: Titanic,Avatar | James Cameron


SELECT string_agg(m.title, ' ,'), m.director
FROM movie m
GROUP BY m.director
HAVING COUNT(m.title) > 1;