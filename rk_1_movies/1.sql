-- # Найти названия всех фильмов снятых ‘Steven Spielberg’, отсортировать по алфавиту.

SELECT title
FROM movie
WHERE director = 'Steven Spielberg';