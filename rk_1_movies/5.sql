-- Напишите запрос возвращающий информацию о рейтингах в более читаемом формате:
-- имя эксперта, название фильма, оценка и дата оценки.
-- Отсортируйте данные по имени эксперта, затем названию фильма и наконец оценка.

SELECT reviewer.name, movie.title, rating.stars, rating.ratingdate
FROM movie
         JOIN rating USING (mid)
         JOIN reviewer USING (rid)
order by reviewer.name, movie.title, rating.stars;