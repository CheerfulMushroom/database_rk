-- Некоторые оценки не имеют даты. Найти имена всех экспертов, имеющих оценки без даты, отсортировать по алфавиту.

-- Решение с подзапросом
SELECT rev.name
FROM reviewer as rev
WHERE rev.rid IN (
    SELECT DISTINCT rating.rid
    FROM rating
    WHERE rating.ratingdate IS NULL
)
ORDER BY rev.name;

-- Решение без подзапроса
SELECT DISTINCT reviewer.name
FROM reviewer
JOIN rating ON rating.rid = reviewer.rid
WHERE rating.ratingdate IS NULL
ORDER BY reviewer.name;