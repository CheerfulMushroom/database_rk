-- Для всех случаев когда один эксперт оценивал фильм более одного раза и указал лучший рейтинг второй раз,
-- выведите имя эксперта и название фильма, отсортировав по имени, затем по названию фильма.

SELECT rvw.name, m.title
FROM rating r1
         JOIN rating r2 USING (mid, rid)
         JOIN movie m USING (mid)
         JOIN reviewer rvw USING (rid)
WHERE r1.rid = r2.rid
  AND r1.ratingdate > r2.ratingdate
  AND r1.stars > r2.stars
ORDER BY rvw.name, m.title;