-- # Найти названия всех фильмов которые не имеют рейтинга, отсортировать по алфавиту.

-- Решение с подзапросом
SELECT DISTINCT m.title
FROM movie AS m
WHERE NOT m.mid IN (
    SELECT DISTINCT rating.mid
    FROM rating
)
ORDER BY m.title;

-- Решение без подзапроса
SELECT title
FROM movie
LEFT JOIN rating on movie.mid = rating.mid
WHERE rating IS NULL
ORDER BY title;
