-- Найти имена всех экспертов, кто оценил “Gone with the Wind”, отсортировать по алфавиту.

SELECT reviewer.name
FROM movie
         JOIN rating USING (mid)
         JOIN reviewer USING (rid)
WHERE movie.title = 'Gone with the Wind'
GROUP BY reviewer.name
ORDER BY reviewer.name;

-- ANOTHER ANSWER
SELECT DISTINCT reviewer.name
FROM movie
         JOIN rating USING (mid)
         JOIN reviewer USING (rid)
WHERE movie.title = 'Gone with the Wind'
ORDER BY reviewer.name;